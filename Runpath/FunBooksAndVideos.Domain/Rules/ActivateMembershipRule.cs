﻿using FunBooksAndVideos.Domain.Models;
using FunBooksAndVideos.Domain.Services;
using System.Linq;

namespace FunBooksAndVideos.Domain.Rules
{
    public class ActivateMembershipRule : IRule
    {
        private readonly IUserService _userService;

        public ActivateMembershipRule(IUserService userService)
        {
            _userService = userService;
        }

        public void Check(Order order)
        {
            if (order.Items.Any(x => x.IsMembership))
            {
                foreach (var item in order.Items)
                {
                    if (item.IsMembership)
                    {
                        _userService.ActivateMembership(item.Id);
                    }
                }
            }
        }
    }
}

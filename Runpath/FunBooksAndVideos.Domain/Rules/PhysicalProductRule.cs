﻿using FunBooksAndVideos.Domain.Models;
using FunBooksAndVideos.Domain.Services;

namespace FunBooksAndVideos.Domain.Rules
{
    public class PhysicalProductRule : IRule
    {
        IShippingService _shippingService;

        public PhysicalProductRule(IShippingService shippingService)
        {
            _shippingService = shippingService;
        }

        public void Check(Order order)
        {
            _shippingService.GenerateShippingSlip(order);
        }
    }
}

﻿namespace FunBooksAndVideos.Domain.Models
{
    public interface IRule
    {
        void Check(Order order);
    }
}

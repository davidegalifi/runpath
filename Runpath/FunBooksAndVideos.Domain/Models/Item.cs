﻿namespace FunBooksAndVideos.Domain.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ItemType Type { get; set; }
        public bool IsMembership { get; set; }
        public bool IsItAPhysicalProduct { get; set; }
    }
}

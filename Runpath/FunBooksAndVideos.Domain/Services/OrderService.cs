﻿using FunBooksAndVideos.Domain.Models;
using FunBooksAndVideos.Domain.Services;
using System.Collections.Generic;

namespace FunBooksAndVideos.Domain.Services
{
    public class OrderService : IOrderService
    {
        IList<IRule> _rules;

        public OrderService(IList<IRule> rules)
        {
            _rules = rules;
        }
        public void Process(Order order)
        {
            ProcessRules(order);
        }

        private void ProcessRules(Order order)
        {
            foreach (var rules in _rules)
            {
                rules.Check(order);
            }
        }
    }
}

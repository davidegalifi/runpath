﻿using FunBooksAndVideos.Domain.Models;

namespace FunBooksAndVideos.Domain.Services
{
    public interface IUserService
    {
        void ActivateMembership(int itemId);
    }
}

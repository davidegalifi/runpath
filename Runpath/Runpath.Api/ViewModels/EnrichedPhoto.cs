﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Runpath.Api.ViewModels
{
    public class EnrichedPhoto
    {
        public int Id { get; set; }

        public string AlbumTitle { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public string ThumbnailUrl { get; set; }

        public int UserId { get; set; }
    }
}

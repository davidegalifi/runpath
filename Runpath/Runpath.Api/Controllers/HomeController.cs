﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Runpath.Api.Clients;
using Runpath.Api.Models;
using Runpath.Api.ViewModels;

namespace Runpath.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly PhotosClient _photosClient;

        public HomeController(PhotosClient photosClient)
        {
            _photosClient = photosClient;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IList<EnrichedPhoto>>> Get()
        {
            IList<Photo> photos = null;
            IList<Album> albums = null;

            
            photos = await _photosClient.GetPhotosAsync();
            
            albums = await _photosClient.GetAlbumsAsync();

            IList<EnrichedPhoto> enPhotos = (from photo in photos
                                             join album in albums on photo.AlbumId equals album.Id
                                             select new EnrichedPhoto
                                             {
                                                 Id = photo.Id,
                                                 Url = photo.Url,
                                                 Title = photo.Title,
                                                 AlbumTitle = album.Title,
                                                 ThumbnailUrl = photo.ThumbnailUrl,
                                                 UserId = album.UserId
                                             }).ToList();

            return new JsonResult(enPhotos);
        }
    }
}

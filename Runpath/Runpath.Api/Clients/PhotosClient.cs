﻿using Newtonsoft.Json;
using Runpath.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Runpath.Api.Clients
{
    public class PhotosClient
    {
        private readonly HttpClient _httpClient;

        public PhotosClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IList<Photo>> GetPhotosAsync()
        {
            var res = await _httpClient.GetAsync("photos");

            var content = await res.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<IList<Photo>>(content);
        }

        public async Task<IList<Album>> GetAlbumsAsync()
        {
            var res = await _httpClient.GetAsync("albums");

            var content = await res.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<IList<Album>>(content);
        }
    }
}
